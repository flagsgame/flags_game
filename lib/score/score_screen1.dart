import 'package:example_project/controllers/question_controller1.dart';
import 'package:example_project/homepage/homepage.dart';
import 'package:example_project/launcher.dart';
import 'package:example_project/login/login.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ScoreScreen extends StatelessWidget {
  final ButtonStyle style =
      ElevatedButton.styleFrom(textStyle: const TextStyle(fontSize: 20));
  final imgBg =
      'https://imgix.bustle.com/uploads/shutterstock/2020/7/2/9cb85b0e-466b-4fe1-b8c0-932a3a090195-shutterstock-1355834585.jpg?w=2000&h=1090&fit=crop&crop=faces&auto=format%2Ccompress&blend=000000&blendAlpha=45&blendMode=normal';
  ScoreScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    QuestionController1 _qnController = Get.put(QuestionController1());
    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.logout),
            tooltip: 'Logout',
            onPressed: () async {
              await Navigator.of(context, rootNavigator: true).pushReplacement(
                  MaterialPageRoute(builder: (context) => new Login()));
            },
          ),
        ],
        title: const Text('Flags Game'),
        automaticallyImplyLeading: false,
      ),
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: NetworkImage(imgBg),
                fit: BoxFit.cover,
              ),
            ),
          ),
          Column(
            children: [
              Spacer(
                flex: 3,
              ),
              Text(
                'Score',
                style: Theme.of(context)
                    .textTheme
                    .headline3!
                    .copyWith(color: Color(0xFFE92E30)),
              ),
              Spacer(),
              Text(
                '${_qnController.numOfCorrectAns * 10}/${_qnController.questions.length * 10}',
                style: Theme.of(context)
                    .textTheme
                    .headline4!
                    .copyWith(color: Color(0xFFE92E30)),
              ),
              Spacer(
                flex: 2,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ElevatedButton(
                    style: style,
                    onPressed: () async {
                      await Navigator.push(context,
                          MaterialPageRoute(builder: (context) => Launcher()));
                    },
                    child: const Text('เล่นอีกครั้ง'),
                  ),
                ],
              ),
              Spacer(
                flex: 6,
              ),
            ],
          )
        ],
      ),
    );
  }
}
