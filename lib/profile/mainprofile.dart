import 'package:example_project/login/login.dart';
import 'package:example_project/profile/profile.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

class MainProfile extends StatefulWidget {
  MainProfile({Key? key}) : super(key: key);

  @override
  _MainProfileState createState() => _MainProfileState();
}

class _MainProfileState extends State<MainProfile> {
  final Future<FirebaseApp> _initalization = Firebase.initializeApp();
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: _initalization,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return Text('Error..');
          } else if (snapshot.connectionState == ConnectionState.done) {
            return Pro();
          }
          return Text('Loading..');
        });
  }
}

class Pro extends StatelessWidget {
  static const String _title = 'Flags Game';
  static const routeName = '/rank';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: _title,
      home: MainPro(),
    );
  }
}

class MainPro extends StatefulWidget {
  MainPro({Key? key}) : super(key: key);

  @override
  _MainProState createState() => _MainProState();
}

class _MainProState extends State<MainPro> {
  @override
  Widget build(BuildContext context) {
    final imgBg =
        'https://imgix.bustle.com/uploads/shutterstock/2020/7/2/9cb85b0e-466b-4fe1-b8c0-932a3a090195-shutterstock-1355834585.jpg?w=2000&h=1090&fit=crop&crop=faces&auto=format%2Ccompress&blend=000000&blendAlpha=45&blendMode=normal';
    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.logout),
            tooltip: 'Logout',
            onPressed: () async {
              await Navigator.of(context, rootNavigator: true).pushReplacement(
                  MaterialPageRoute(builder: (context) => new Login()));
            },
          ),
        ],
        title: const Text('Flags Game'),
        automaticallyImplyLeading: false,
      ),
      body: Container(
        width: double.infinity,
        height: double.infinity,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: NetworkImage(imgBg),
            fit: BoxFit.cover,
          ),
        ),
        child: Profile(),
      ),
    );
  }
}
