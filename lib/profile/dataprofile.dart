import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class DataProfile extends StatefulWidget {
  String userId;
  DataProfile({Key? key, required this.userId}) : super(key: key);

  @override
  _DataProfileState createState() => _DataProfileState(this.userId);
}

class _DataProfileState extends State<DataProfile> {
  final imgBg =
      'https://imgix.bustle.com/uploads/shutterstock/2020/7/2/9cb85b0e-466b-4fe1-b8c0-932a3a090195-shutterstock-1355834585.jpg?w=2000&h=1090&fit=crop&crop=faces&auto=format%2Ccompress&blend=000000&blendAlpha=45&blendMode=normal';
  String userId;
  String name = "";
  String age = "0";
  String highscore = "0";
  CollectionReference users = FirebaseFirestore.instance.collection('users');
  _DataProfileState(this.userId);
  TextEditingController _nameController = new TextEditingController();
  TextEditingController _highscoreController = new TextEditingController();
  TextEditingController _ageController = new TextEditingController();
  @override
  void initState() {
    super.initState();
    if (this.userId.isNotEmpty) {
      users.doc(this.userId).get().then((snapshot) {
        if (snapshot.exists) {
          var data = snapshot.data() as Map<String, dynamic>;
          name = data['name'];
          highscore = data['highscore'].toString();
          age = data['age'];
          _nameController.text = name;
          _highscoreController.text = highscore;
          _ageController.text = age;
        }
      });
    }
  }

  Future<void> updateUser() {
    return users
        .doc(this.userId)
        .update({
          'name': this.name,
          'highscore': int.parse(this.highscore),
          'age': this.age,
        })
        .then((value) => print("User Updated"))
        .catchError((error) => print("Failed to update user: $error"));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('User')),
      body: Container(
        width: double.infinity,
        height: double.infinity,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: NetworkImage(imgBg),
            fit: BoxFit.cover,
          ),
        ),
        padding: EdgeInsets.fromLTRB(10, 100, 10, 0),
        child: Form(
          autovalidateMode: AutovalidateMode.onUserInteraction,
          child: Column(
            children: [
              Text('Edit your name',
                  style: Theme.of(context).textTheme.headline6!.copyWith(
                      color: Colors.white,
                      fontWeight: FontWeight.normal,
                      fontSize: 30)),
              TextFormField(
                style: TextStyle(color: Colors.red),
                controller: _nameController,
                decoration: InputDecoration(
                    labelText: 'Name',
                    labelStyle: TextStyle(color: Colors.yellow, fontSize: 30)),
                onChanged: (value) {
                  setState(() {
                    name = value;
                  });
                },
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please input Name';
                  }
                  return null;
                },
              ),
              Text('Enter your Age',
                  style: Theme.of(context).textTheme.headline6!.copyWith(
                      color: Colors.white,
                      fontWeight: FontWeight.normal,
                      fontSize: 30)),
              TextFormField(
                style: TextStyle(color: Colors.red),
                controller: _ageController,
                decoration: InputDecoration(
                    labelText: 'Age',
                    labelStyle: TextStyle(color: Colors.yellow, fontSize: 30)),
                onChanged: (value) {
                  setState(() {
                    age = value;
                  });
                },
                validator: (value) {
                  if (value == null ||
                      value.isEmpty ||
                      int.tryParse(value) == null) {
                    return 'Please input Age';
                  }
                  return null;
                },
              ),
              Padding(padding: EdgeInsets.fromLTRB(0, 10, 0, 0)),
              ElevatedButton(
                onPressed: () async {
                  await updateUser();
                  Navigator.pop(context);
                },
                child: Text(
                  'Save',
                  style: TextStyle(
                    fontSize: 30,
                  ),
                ),
                style: ElevatedButton.styleFrom(
                    fixedSize: const Size(100, 50), primary: Colors.deepOrange),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
