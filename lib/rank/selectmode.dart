import 'package:example_project/login/login.dart';
import 'package:example_project/rank/rank1.dart';
import 'package:example_project/rank/rank2.dart';
import 'package:flutter/material.dart';

class SelectMode extends StatefulWidget {
  SelectMode({Key? key}) : super(key: key);

  @override
  _SelectModeState createState() => _SelectModeState();
}

class _SelectModeState extends State<SelectMode> {
  static const String _title = 'Flags Game';
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: _title,
      home: Select(),
    );
  }
}

class Select extends StatefulWidget {
  Select({Key? key}) : super(key: key);

  @override
  _SelectState createState() => _SelectState();
}

class _SelectState extends State<Select> {
  @override
  Widget build(BuildContext context) {
    final imgBg =
        'https://imgix.bustle.com/uploads/shutterstock/2020/7/2/9cb85b0e-466b-4fe1-b8c0-932a3a090195-shutterstock-1355834585.jpg?w=2000&h=1090&fit=crop&crop=faces&auto=format%2Ccompress&blend=000000&blendAlpha=45&blendMode=normal';
    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.logout),
            tooltip: 'Logout',
            onPressed: () async {
              await Navigator.of(context, rootNavigator: true).pushReplacement(
                  MaterialPageRoute(builder: (context) => new Login()));
            },
          ),
        ],
        title: const Text('Flags Game'),
        automaticallyImplyLeading: false,
      ),
      body: Stack(
        children: <Widget>[
          Container(
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: NetworkImage(imgBg),
                fit: BoxFit.cover,
              ),
            ),
          ),
          Center(
            child: Column(
              children: <Widget>[
                Spacer(),
                SizedBox(
                  width: 300,
                  height: 150,
                  child: ElevatedButton(
                    onPressed: () async {
                      await Navigator.push(context,
                          MaterialPageRoute(builder: (context) => ShowRank1()));
                    },
                    style: ElevatedButton.styleFrom(
                      primary: Colors.greenAccent,
                    ),
                    child: const Text('Rank Mode1',
                        style: TextStyle(fontSize: 40, color: Colors.purple)),
                  ),
                ),
                Spacer(),
                SizedBox(
                  width: 300,
                  height: 150,
                  child: ElevatedButton(
                    onPressed: () async {
                      await Navigator.push(context,
                          MaterialPageRoute(builder: (context) => ShowRank2()));
                    },
                    style: ElevatedButton.styleFrom(
                      primary: Colors.lightBlueAccent,
                    ),
                    child: const Text(
                      'Rank Mode2',
                      style: TextStyle(fontSize: 40, color: Colors.red),
                    ),
                  ),
                ),
                Spacer(),
              ],
            ),
          )
        ],
      ),
    );
  }
}
