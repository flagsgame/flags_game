import 'package:example_project/login/login.dart';
import 'package:example_project/rank/datarank2.dart';
import 'package:example_project/rank/rank1.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

class ShowRank2 extends StatefulWidget {
  ShowRank2({Key? key}) : super(key: key);

  @override
  _ShowRank2State createState() => _ShowRank2State();
}

class _ShowRank2State extends State<ShowRank2> {
  final Future<FirebaseApp> _initalization = Firebase.initializeApp();
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: _initalization,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return Text('Error..');
          } else if (snapshot.connectionState == ConnectionState.done) {
            return Rank();
          }
          return Text('Loading..');
        });
  }
}

class Rank extends StatelessWidget {
  static const String _title = 'Flags Game';
  static const routeName = '/rank';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: _title,
      home: MainHome(),
    );
  }
}

class MainHome extends StatefulWidget {
  MainHome({Key? key}) : super(key: key);

  @override
  _MainHomeState createState() => _MainHomeState();
}

class _MainHomeState extends State<MainHome> {
  @override
  Widget build(BuildContext context) {
    final imgBg =
        'https://imgix.bustle.com/uploads/shutterstock/2020/7/2/9cb85b0e-466b-4fe1-b8c0-932a3a090195-shutterstock-1355834585.jpg?w=2000&h=1090&fit=crop&crop=faces&auto=format%2Ccompress&blend=000000&blendAlpha=45&blendMode=normal';
    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.logout),
            tooltip: 'Logout',
            onPressed: () async {
              await Navigator.of(context, rootNavigator: true).pushReplacement(
                  MaterialPageRoute(builder: (context) => new Login()));
            },
          ),
        ],
        title: const Text('Flags Game'),
        automaticallyImplyLeading: false,
      ),
      body: Column(
        children: <Widget>[
          Flexible(
            child: Container(
              width: double.infinity,
              height: double.infinity,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: NetworkImage(imgBg),
                  fit: BoxFit.cover,
                ),
              ),
              child: Column(
                children: [
                  Column(
                    children: [
                      Text(
                        'Rank Mode2',
                        style: TextStyle(
                            fontSize: 40,
                            color: Colors.redAccent,
                            fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                  DataRank2(),
                  Spacer(),
                  Column(
                    children: [
                      SizedBox(
                        width: 300,
                        height: 150,
                        child: ElevatedButton(
                          onPressed: () async {
                            await Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ShowRank1()));
                          },
                          style: ElevatedButton.styleFrom(
                            primary: Colors.greenAccent,
                          ),
                          child: const Text(
                            'Rank Mode1',
                            style:
                                TextStyle(fontSize: 40, color: Colors.purple),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Spacer()
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
