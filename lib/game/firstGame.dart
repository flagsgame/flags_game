import 'package:example_project/controllers/question_controller1.dart';
import 'package:example_project/game/progress_bar1.dart';
import 'package:example_project/game/question_card1.dart';
import 'package:example_project/login/login.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

void main() => runApp(FirstGame());

class FirstGame extends StatelessWidget {
  static const String _title = 'Flags Game';
  static const routeName = '/firstgame';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: _title,
      home: MainHome(),
    );
  }
}

class MainHome extends StatefulWidget {
  MainHome({Key? key}) : super(key: key);

  @override
  _MainHomeState createState() => _MainHomeState();
}

class _MainHomeState extends State<MainHome> {
  QuestionController1 _questionController = Get.put(QuestionController1());
  @override
  Widget build(BuildContext context) {
    final imgBg =
        'https://imgix.bustle.com/uploads/shutterstock/2020/7/2/9cb85b0e-466b-4fe1-b8c0-932a3a090195-shutterstock-1355834585.jpg?w=2000&h=1090&fit=crop&crop=faces&auto=format%2Ccompress&blend=000000&blendAlpha=45&blendMode=normal';
    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.logout),
            tooltip: 'Logout',
            onPressed: () async {
              await Navigator.of(context, rootNavigator: true).pushReplacement(
                  MaterialPageRoute(builder: (context) => new Login()));
            },
          ),
        ],
        title: const Text('Flags Game'),
        automaticallyImplyLeading: false,
      ),
      body: Stack(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: NetworkImage(imgBg),
                fit: BoxFit.cover,
              ),
            ),
          ),
          SafeArea(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 10),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: ProgressBar(),
              ),
              SizedBox(
                height: 5,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Obx(() => Text.rich(TextSpan(
                        text:
                            'Mode_1 Flag ${_questionController.questionNumber.value}',
                        style: Theme.of(context)
                            .textTheme
                            .headline4!
                            .copyWith(color: Color(0xFFCCFB5D)),
                        children: [
                          TextSpan(
                              text: '/${_questionController.questions.length}',
                              style: Theme.of(context)
                                  .textTheme
                                  .headline5!
                                  .copyWith(color: Color(0xFFCCFB5D)))
                        ]))),
              ),
              Divider(
                thickness: 1.5,
                color: Colors.red,
              ),
              SizedBox(
                height: 10,
              ),
              Expanded(
                  child: PageView.builder(
                physics: NeverScrollableScrollPhysics(),
                controller: _questionController.pageController,
                onPageChanged: _questionController.updateTheQnNum,
                itemCount: _questionController.questions.length,
                itemBuilder: (context, index) => QuestionCard(
                  question: _questionController.questions[index],
                ),
              ))
            ],
          )),
        ],
      ),
    );
  }
}
