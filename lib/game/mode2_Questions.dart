class Question {
  final int id, answer;
  final String question;
  final List<String> options;

  Question(
      {required this.id,
      required this.question,
      required this.answer,
      required this.options});
}

const List sample_data = [
  {
    "id": 1,
    "question": "Wales",
    "options": [
      'https://sv1.picz.in.th/images/2021/10/29/uoWaja.th.png',
      'https://sv1.picz.in.th/images/2021/10/29/uoWGBz.th.png',
      'https://sv1.picz.in.th/images/2021/10/28/ulK2yn.th.png',
      'https://sv1.picz.in.th/images/2021/10/28/ulKFB8.th.png'
    ],
    "answer_index": 2,
  },
  {
    "id": 2,
    "question": "Venezuela",
    "options": [
      'https://sv1.picz.in.th/images/2021/10/29/uoWGBz.th.png',
      'https://sv1.picz.in.th/images/2021/10/28/ulZ4le.th.png',
      'https://sv1.picz.in.th/images/2021/10/29/uod0ve.th.png',
      'https://sv1.picz.in.th/images/2021/10/28/ulcYRD.th.png'
    ],
    "answer_index": 0,
  },
  {
    "id": 3,
    "question": "Brazil",
    "options": [
      'https://sv1.picz.in.th/images/2021/10/28/ulZ4le.th.png',
      'https://sv1.picz.in.th/images/2021/10/29/uoWaja.th.png',
      'https://sv1.picz.in.th/images/2021/10/29/uodN7E.th.png',
      'https://sv1.picz.in.th/images/2021/10/29/uodz8v.th.png'
    ],
    "answer_index": 3,
  },
  {
    "id": 4,
    "question": "Costa_Rica",
    "options": [
      'https://sv1.picz.in.th/images/2021/10/28/ulZ4le.th.png',
      'https://sv1.picz.in.th/images/2021/10/29/uodf2V.th.png',
      'https://sv1.picz.in.th/images/2021/10/29/uodXuN.th.png',
      'https://sv1.picz.in.th/images/2021/10/29/uodKH1.th.png'
    ],
    "answer_index": 1,
  },
  {
    "id": 5,
    "question": "Kenya",
    "options": [
      'https://sv1.picz.in.th/images/2021/10/28/ulZ4le.th.png',
      'https://sv1.picz.in.th/images/2021/10/29/uodXuN.th.png',
      'https://sv1.picz.in.th/images/2021/10/29/uoWaja.th.png',
      'https://sv1.picz.in.th/images/2021/10/28/ul1aee.th.png'
    ],
    "answer_index": 1,
  },
  {
    "id": 6,
    "question": "Argentina",
    "options": [
      'https://sv1.picz.in.th/images/2021/10/28/ulc3kq.th.png',
      'https://sv1.picz.in.th/images/2021/10/28/ulKFB8.th.png',
      'https://sv1.picz.in.th/images/2021/10/29/uoWaja.th.png',
      'https://sv1.picz.in.th/images/2021/10/28/ulZ4le.th.png'
    ],
    "answer_index": 2,
  },
  {
    "id": 7,
    "question": "Portugal",
    "options": [
      'https://sv1.picz.in.th/images/2021/10/29/uo7pvn.th.png',
      'https://sv1.picz.in.th/images/2021/10/29/uo7bc0.th.png',
      'https://sv1.picz.in.th/images/2021/10/28/ulclWV.th.png',
      'https://sv1.picz.in.th/images/2021/10/29/uoSwft.th.png'
    ],
    "answer_index": 1,
  },
  {
    "id": 8,
    "question": "Iran",
    "options": [
      'https://sv1.picz.in.th/images/2021/10/29/uoSwft.th.png',
      'https://sv1.picz.in.th/images/2021/10/29/uoWaja.th.png',
      'https://sv1.picz.in.th/images/2021/10/29/uodf2V.th.png',
      'https://sv1.picz.in.th/images/2021/10/29/uodz8v.th.png'
    ],
    "answer_index": 0,
  },
  {
    "id": 9,
    "question": "Cameroon",
    "options": [
      'https://sv1.picz.in.th/images/2021/10/28/ulc3kq.th.png',
      'https://sv1.picz.in.th/images/2021/10/29/uo7bc0.th.png',
      'https://sv1.picz.in.th/images/2021/10/29/uoWaja.th.png',
      'https://sv1.picz.in.th/images/2021/10/29/uodN7E.th.png'
    ],
    "answer_index": 3,
  },
  {
    "id": 10,
    "question": "Norway",
    "options": [
      'https://sv1.picz.in.th/images/2021/10/29/uoSwft.th.png',
      'https://sv1.picz.in.th/images/2021/10/28/ulZbwf.th.png',
      'https://sv1.picz.in.th/images/2021/10/28/ulcbnN.th.png',
      'https://sv1.picz.in.th/images/2021/10/28/ulcYRD.th.png'
    ],
    "answer_index": 3,
  },
];
