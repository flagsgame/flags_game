import 'package:example_project/controllers/question_controller2.dart';
import 'package:example_project/game/option2.dart';
import 'package:flutter/material.dart';
import 'package:example_project/game/mode2_Questions.dart';
import 'package:get/get.dart';

class QuestionCard extends StatelessWidget {
  const QuestionCard({
    Key? key,
    required this.question,
  }) : super(key: key);
  final Question question;
  @override
  Widget build(BuildContext context) {
    QuestionController2 _controller = Get.put(QuestionController2());
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 10),
      padding: EdgeInsets.all(20),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
      ),
      child: Column(
        children: [
          Text(
            question.question,
            style: Theme.of(context)
                .textTheme
                .headline6!
                .copyWith(color: Color(0xFF101010)),
          ),
          // Image.network(question.question,
          //     width: 200, height: 100, fit: BoxFit.fill),
          SizedBox(
            height: 10 / 2,
          ),
          ...List.generate(
              question.options.length,
              (index) => Option(
                  text: question.options[index],
                  index: index,
                  press: () => _controller.checkAns(question, index)))
        ],
      ),
    );
  }
}
