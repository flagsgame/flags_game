class Question {
  final int id, answer;
  final String question;
  final List<String> options;

  Question(
      {required this.id,
      required this.question,
      required this.answer,
      required this.options});
}

const List sample_data = [
  {
    "id": 1,
    "question": "https://sv1.picz.in.th/images/2021/10/28/ul1aee.th.png",
    "options": ['India', 'Thailand', 'England', 'Canada'],
    "answer_index": 1,
  },
  {
    "id": 2,
    "question": "https://sv1.picz.in.th/images/2021/10/28/ulK2yn.th.png",
    "options": ['China', 'Vietnam', 'Wales', 'Belgium'],
    "answer_index": 2,
  },
  {
    "id": 3,
    "question": "https://sv1.picz.in.th/images/2021/10/28/ulKFB8.th.png",
    "options": ['Finland', 'Canada', 'Switzerland', 'Mexico'],
    "answer_index": 2,
  },
  {
    "id": 4,
    "question": "https://sv1.picz.in.th/images/2021/10/29/uo7pvn.th.png",
    "options": ['Saudi Arabia', 'India', 'Vietnam', 'Cambodia'],
    "answer_index": 0,
  },
  {
    "id": 5,
    "question": "https://sv1.picz.in.th/images/2021/10/28/ulZwIS.th.png",
    "options": ['Russia', 'Finland', 'Belgium', 'Romania'],
    "answer_index": 3,
  },
  {
    "id": 6,
    "question": "https://sv1.picz.in.th/images/2021/10/28/ulZbwf.th.png",
    "options": ['Poland', 'Canada', 'Peru', 'France'],
    "answer_index": 0,
  },
  {
    "id": 7,
    "question": "https://sv1.picz.in.th/images/2021/10/28/ulcYRD.th.png",
    "options": ['Switzerland', 'Spain', 'Norway', 'Poland'],
    "answer_index": 2,
  },
  {
    "id": 8,
    "question": "https://sv1.picz.in.th/images/2021/10/28/ulc3kq.th.png",
    "options": ['India', 'Belgium', 'Sweden', 'Romania'],
    "answer_index": 0,
  },
  {
    "id": 9,
    "question": "https://sv1.picz.in.th/images/2021/10/28/ulcbnN.th.png",
    "options": ['Sweden', 'Canada', 'Poland', 'Cambodia'],
    "answer_index": 1,
  },
  {
    "id": 10,
    "question": "https://sv1.picz.in.th/images/2021/10/28/ulclWV.th.png",
    "options": ['Taiwan', 'India', 'Bolivia', 'Belgium'],
    "answer_index": 3,
  },
];
